<?php
include_once 'db.inc.php';

class NewProduct extends Database {

    public function getValues() {
        $sku = $this->escapeStr($_POST['sku']);
        $name = $this->escapeStr($_POST['name']);
        $price = $this->escapeStr($_POST['price']);
        $type = $this->escapeStr($_POST['type']);

        if ($type == 'disc') {
            $attribute = $this->escapeStr('Size: '.$_POST['size'].' MB');
        } else if ($type == 'book') {
            $attribute = $this->escapeStr('Weight: '.$_POST['weight'].' Kg');
        } else if ($type == 'furniture') {
            $height = $_POST['height'];
            $width = $_POST['width'];
            $length = $_POST['length'];
            $attribute = $this->escapeStr('Dimensions: '.$height.'x'.$width.'x'.$length);
        }

        $this->addProduct($sku, $name, $price, $type, $attribute);
    }

    private function addProduct($sku, $name, $price, $type, $attribute) {
        $sql = "INSERT INTO products (sku, name, price, type, attribute) VALUES ('$sku', '$name', '$price', '$type', '$attribute')";
        $result = $this->connect()->query($sql);
        if($result == true) {
            header('Location: ../index.php');
        } else {
            echo 'Error: Cannot add new product';
        }
    }
}

if(isset($_POST['add-btn'])) {
    $a = new NewProduct();
    $a->getValues();
}

if (isset($_POST['add-product'])) {
    header('Location: ../add_product.html');
}

?>