<?php
include_once 'db.inc.php';

class Delete extends Database {

    public function getDeleteIds() {
        if (isset($_POST['checkbox'])) {
            $delete = $_POST['checkbox'];
            $ids = implode(',', $delete);
            $ids = $this->escapeStr($ids);
            $this->deleteIds($ids);
        }
    }

    private function deleteIds($ids) {
        $sql = "DELETE FROM products WHERE id IN ($ids)";
        $result = $this->connect()->query($sql);
        if($result == true) {
            header('Location: ../index.php');
        } else {
            echo 'Error: This product cannot be deleted';
        }
    }
}

if(isset($_POST['delete-btn'])) {
    $a = new Delete();
    $a->getDeleteIds();
}