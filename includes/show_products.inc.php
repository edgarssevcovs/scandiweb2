<?php
include_once 'db.inc.php';

class viewProducts extends Database {
    
    private function getProducts() {
        $sql = "SELECT * FROM products";
        $result = $this->connect()->query($sql);
        $rows = $result->num_rows;
        if($rows > 0) {
            while($data = $result->fetch_assoc()) {
                $product[] = $data;
            }
            return $product;
        }
    }

    public function showProducts() {
        $products = $this->getProducts();
        foreach($products as $product) { ?>
            <div class = 'item'>
                <input type = 'checkbox' name = 'checkbox[]' value = "<?= $product['id'] ?>">
                <div class = 'item-description'>
                    <?= $product['sku']; ?>
                </div>
                <div class = 'item-description'>
                    <?= $product['name']; ?>
                </div>
                <div class = 'item-description'>
                    <?= $product['price']; ?> &euro;
                </div>
                <div class = 'item-description'>
                    <?= $product['attribute']; ?>
                </div>
            </div><?php
        }
    }
}

?>
