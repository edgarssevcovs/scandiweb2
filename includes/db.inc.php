<?php

class Database {

    private $server;
    private $user;
    private $password;
    private $db;

    protected function connect() {
        $this->server = "localhost";
        $this->user = "root";
        $this->password = "";
        $this->db = "scandiweb";

        $conn = new mysqli($this->server, $this->user, $this->password, $this->db);
        return $conn;
    }

    protected function escapeStr($value) {
        return $this->connect()->real_escape_string($value);
    }
}

?>