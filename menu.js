a = document.getElementById('menu');
btn = document.getElementById('btn');
form = document.getElementById('action');
a.addEventListener('change', function () {
var selected = a.value;
    switch(selected) {
        case 'add' :
            btn.name = "add-product";
            form.action = 'includes/new_product.inc.php';
            break;
        case 'delete' :
            btn.name = "delete-btn";
            form.action = 'includes/delete_product.inc.php';
            break;
        default:
            btn.name = "add-product";
            form.action = 'includes/new_product.inc.php';
    }
});