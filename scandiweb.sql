-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2019 at 01:35 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `attribute`) VALUES
(20, 'JVC200123', 'Acme DISC', 1, 'disc', 'Size: 700 MB'),
(21, 'JVC200123', 'Acme DISC', 1, 'disc', 'Size: 700 MB'),
(22, 'JVC200123', 'Acme DISC', 1, 'disc', 'Size: 700 MB'),
(23, 'JVC200123', 'Acme DISC', 1, 'disc', 'Size: 700 MB'),
(24, 'GGWP0007', 'War and Peace', 20, 'book', 'Weight: 2 Kg'),
(25, 'GGWP0007', 'War and Peace', 20, 'book', 'Weight: 2 Kg'),
(26, 'GGWP0007', 'War and Peace', 20, 'book', 'Weight: 2 Kg'),
(27, 'GGWP0007', 'War and Peace', 20, 'book', 'Weight: 2 Kg'),
(28, 'TR120555', 'Chair', 40, 'furniture', 'Dimensions: 24x45x15'),
(29, 'TR120555', 'Chair', 40, 'furniture', 'Dimensions: 24x45x15'),
(30, 'TR120555', 'Chair', 40, 'furniture', 'Dimensions: 24x45x15'),
(31, 'TR120555', 'Chair', 40, 'furniture', 'Dimensions: 24x45x15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
