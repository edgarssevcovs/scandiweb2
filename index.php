<?php
    include 'includes/show_products.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Scandiweb</title>
</head>
<body>
    <div class="content">
        <div class="top">
            <div class="top-left">
                Product List
            </div>
            <div class="top-right">
                <div class = "menu">
                    <select name = "menu" id = "menu">
                        <option value = "add">Add Product</option>
                        <option value = "delete">Mass Delete</option>
                    </select>
                </div>
                <div class = "menu">
                    <form action = "includes/new_product.inc.php" method = "POST" id = "action">
                        <button type = "submit"  class = "btn-green" name = "add-product" id = "btn">APPLY</button>
                </div>
            </div>
        </div>
        <div class="products">
            <?php 
            $products = new viewProducts();
            $products->showProducts();
            ?>
        </form>
        </div>
    </div>
    <script src = "/menu.js"></script>
</body>
</html>